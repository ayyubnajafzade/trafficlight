class LightControl {
  constructor(id) {
    this.element = document.getElementById(id);

    this.redLight = this.element.querySelector(".red");
    this.yellowLight = this.element.querySelector(".yellow");
    this.greenLight = this.element.querySelector(".green");
    this.turnLight = this.element.querySelector(".turn-light");
    this.pedestrianLightX = this.element.querySelector(".p-light-x");
    this.pedestrianLightY = this.element.querySelector(".p-light-y");
  }

  turnOffLight(element) {
    element?.classList?.remove("opacity-100");
    element?.classList?.add("opacity-10");
  }

  turnOnLight(element) {
    element?.classList?.remove("opacity-10");
    element?.classList?.add("opacity-100");
  }

  handleLight(element, on) {
    if (on === true) {
      this.turnOnLight(element);
    } else {
      this.turnOffLight(element);
    }
  }

  red(on = true) {
    this.handleLight(this.redLight, on);
  }

  yellow(on = true) {
    this.handleLight(this.yellowLight, on);
  }

  green(on = true) {
    this.handleLight(this.greenLight, on);
  }

  turn(on = true) {
    this.handleLight(this.turnLight, on);
  }

  pedestrianX(on = true) {
    this.handleLight(this.pedestrianLightX, on);
  }

  pedestrianY(on = true) {
    this.handleLight(this.pedestrianLightY, on);
  }
}

class Road {
  constructor(id, leftControl, rightControl) {
    this.id = id;
    this.element = document.getElementById(id);
    this.leftControl = leftControl;
    this.rightControl = rightControl;

    this.pedestrianLines = this.element.querySelectorAll(".p-line");
  }

  handleBoth(light, on = true) {
    this.leftControl[light](on);
    this.rightControl[light](on);
  }

  handlePedestrianLine(on = true) {
    console.log(this.pedestrianLines.length);

    this.pedestrianLines.forEach((el) => {
      if (on === true) {
        el.classList.remove("opacity-10");
        el.classList.add("opacity-100");
      } else {
        el.classList.remove("opacity-100");
        el.classList.add("opacity-10");
      }
    });
  }

  main(on = true) {
    this.handleBoth("green");
  }

  light(light, on = true) {
    this.handleBoth(light, on);
  }
}

class Main {
  constructor(xRoad, yRoad) {
    this.xRoad = xRoad;
    this.yRoad = yRoad;

    this.mainRoad = xRoad;
    this.otherRoad = yRoad;

    this.isX = true;

    this.step = 0;
  }

  sleep(ms = 2000) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  getDirection(invert = false) {
    return this.isX !== invert ? "X" : "Y";
  }

  step1() {
    this.mainRoad.light("red", false);

    this.otherRoad.light(`pedestrian${this.getDirection(true)}`, false);
    this.mainRoad.light(`pedestrian${this.getDirection(true)}`, false);

    this.mainRoad.light("green");
    this.otherRoad.light("red");

    this.mainRoad.light(`pedestrian${this.getDirection()}`);
    this.otherRoad.handlePedestrianLine();

    this.otherRoad.light(`pedestrian${this.getDirection()}`);
  }

  step2() {
    this.mainRoad.light("green", false);
    this.mainRoad.light("yellow");
  }

  step3() {
    this.mainRoad.light("yellow", false);
    this.mainRoad.light("red");
  }

  step4() {
    this.mainRoad.light("turn");
  }

  step5() {
    this.mainRoad.light("turn", false);

    this.mainRoad.light(`pedestrian${this.getDirection()}`, false);
    this.otherRoad.light(`pedestrian${this.getDirection()}`, false);
    this.otherRoad.handlePedestrianLine(false);
  }

  nextStep() {
    if (this.step < 5) {
      this.step++;

      document.querySelector("#step").textContent = `NEXT ${this.step}`;

      this[`step${this.step}`]();
    }

    if (this.step === 5) {
      const temp = this.otherRoad;

      this.otherRoad = this.mainRoad;
      this.mainRoad = temp;

      this.isX = !this.isX;

      this.step = 0;
    }
  }
}

const xLeft = new LightControl("x-light-left");
const xRight = new LightControl("x-light-right");

const yLeft = new LightControl("y-light-left");
const yRight = new LightControl("y-light-right");

const xRoad = new Road("x-road", xLeft, xRight);
const yRoad = new Road("y-road", yLeft, yRight);

const app = new Main(xRoad, yRoad);

let loopInterval = null;

function next() {
    clearInterval(loopInterval);
    loopInterval = null;

    app.nextStep();
}

function loop() {
    if (loopInterval === null) {
        app.nextStep();

        loopInterval = setInterval(()=>{
            app.nextStep();
        }, 2000)
    } 
    else {
        clearInterval(loopInterval);
        loopInterval = null;
    }
}
